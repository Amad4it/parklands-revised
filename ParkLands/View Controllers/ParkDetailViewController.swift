//
//  ParkDetailViewController.swift
//  ParkLands
//
//  Created by Jason Khong on 11/12/15.
//  Copyright © 2015 ApptivityLab. All rights reserved.
//

import UIKit

class ParkDetailViewController: UIViewController {
    var park: Park!

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.nameLabel.text = self.park.name
        self.coverImageView.image = self.park.photo
        self.ratingLabel.text = "\(self.park.averageRating())"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
